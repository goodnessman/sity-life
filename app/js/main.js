
var showItems = 6;

// events page
function loadMoreEvents(e) {
    e.preventDefault();
    showItems = showItems + 3;
    showEvents(showItems);
}

function showEvents(count) {
    var items = $('#eventBlock li'),
    i = 0;


    items.each(function(index, el) {
        i++;
        if (i <= count) {
            $(el).css('display','inline-block');
        }
    });
}







$(document).ready(function() {
    var arrowScrollBtn = $('.arrow-scroll-btn');
    var headerSidebar = $('.site-header .site-header__sidebar-menu');
    var headerStickyBtnToggle = $('.site-header .site-header__sticky-btn-toggle');
    var sideBarDropdown = $('.site-header__sidebar-dropdown');
    var sideBarCloseBtn = $('.site-header__hidden-area-close');

    $(window).on('scroll', function(event) {
        if ($(this).scrollTop() > 35) {
            arrowScrollBtn.fadeIn(1000);
        } else {
            arrowScrollBtn.fadeOut(1000);
        }
    });
    arrowScrollBtn.on('click', function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
    headerStickyBtnToggle.on('click', function(event) {
        event.preventDefault();
        $(this).parents('.site-header__sidebar-menu').addClass('active');
        headerSidebar.find('.site-header__hidden-area').addClass('active');
        arrowScrollBtn.hide();
    });
    sideBarCloseBtn.on('click', function(event) {
        event.preventDefault();
        $(this).parents('.site-header__sidebar-menu').removeClass('active');
        headerSidebar.find('.site-header__hidden-area').removeClass('active');
    });
    sideBarDropdown.styler();



    // event page
    if ($('#eventPage').length > 0) {
        

        showEvents(showItems);

        $('#eventSlider').slick({
            arrows: false,
            dots: true,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            customPaging: function(slider, i) {
                return '<button></button>';
            }
        });
    }
});
